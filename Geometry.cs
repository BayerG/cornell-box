﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Windows.Media.Media3D;
using Microsoft.SqlServer.Server;

namespace Editor3D
{
    public class Geometry
    {
        public static double GetAngle(double sin, double cos)
        {
            if (cos >= 0)
                return Math.Asin(sin);
            else if (sin >= 0)
                return Math.PI - Math.Asin(sin);
            else
                return -Math.PI - Math.Asin(sin);
        }

        public static Vector3D FacetNormal(Facet facet, Polyhedron polyhedron)
        {
            Vector3D normal;
            bool isConvex = !polyhedron.name.Equals("Rotation Figure");
            if (isConvex)
                normal = Geometry.FacetNormal(facet, polyhedron.Center);
            else
            {
                normal = Geometry.FacetNormal(facet);
                if (facet == polyhedron.facets[polyhedron.facets.Count - 1])
                    normal = -1 * normal;
            }
            return normal;
        }
        
        // возвращает неопределенную нормаль
        public static Vector3D FacetNormal(Facet facet)
        {
            Point3D P0 = facet.points[0];
            Point3D P1 = facet.points[1];
            Point3D P2 = facet.points[2];
            Vector3D P1P0 = new Vector3D(P0.X - P1.X, P0.Y - P1.Y, P0.Z - P1.Z);
            Vector3D P2P0 = new Vector3D(P0.X - P2.X, P0.Y - P2.Y, P0.Z - P2.Z);

            return -Vector3D.CrossProduct(P1P0, P2P0);
        }

        // возвращает истинную нормаль, для выпуклых фигур
        public static Vector3D FacetNormal(Facet facet, Point3D center)
        {
            Point3D P0 = facet.points[0];
            Point3D P1 = facet.points[1];
            Point3D P2 = facet.points[2];
            Vector3D P1P0 = new Vector3D(P0.X - P1.X, P0.Y - P1.Y, P0.Z - P1.Z);
            Vector3D P2P0 = new Vector3D(P0.X - P2.X, P0.Y - P2.Y, P0.Z - P2.Z);

            Vector3D result = Vector3D.CrossProduct(P1P0, P2P0);

            Vector3D P0CENTER = new Vector3D(center.X - P0.X, center.Y - P0.Y, center.Z - P0.Z);
            if (Vector3D.AngleBetween(result, P0CENTER) < 90)
            {
                result *= -1;
            }
            return result;
        }
        
        public static Vector3D SphereNormal(Point3D point, Sphere sphere)
        {
            return new Vector3D(point.X - sphere.X, point.Y - sphere.Y, point.Z - sphere.Z);
        }

        public static Point3D FacetIntersection(Point3D origin, Point3D direction, Facet facet, Polyhedron polyhedron)
        {
            // Общее уравнение прямой Ax + By + Cz + D = 0, где (A, B, C) - нормаль к прямой.
            Vector3D normal = FacetNormal(facet, polyhedron);
            double D = -(normal.X * facet.points[0].X + normal.Y * facet.points[0].Y + normal.Z * facet.points[0].Z);
            
            // Параметрическое уравнение прямой (x0 + tx, y0 + ty, z0 + tz) - прямая, соединающая точки origin и
            // direction, где (x0, y0, z0) - origin, (x, y, z) - direction.
            double denominator = normal.X * direction.X + normal.Y * direction.Y + normal.Z * direction.Z;
            // Проверить, что прямая не параллельна плоскости.
            if (Math.Abs(denominator) < double.Epsilon)
                return null;
            double t = -(normal.X * origin.X + normal.Y * origin.Y + normal.Z * origin.Z + D) / denominator;
            // Проверить, что искомая точка лежит дальше по направлению вектора.
            if (t < 0)
                return null;
            
            // Искомая точка пересечения с плоскостью.
            Point3D intersectionPoint
                = new Point3D(origin.X + t * direction.X, origin.Y + t * direction.Y, origin.Z + t * direction.Z);
            // Проверить, что точка пересечения лежит внутри грани.
            if (Geometry.PointInFacet(intersectionPoint, facet))
                return intersectionPoint;
            else
                return null;
        }

        public static Point3D SphereIntersection(Point3D origin, Point3D direction, Sphere sphere)
        {
            double u = origin.X - sphere.X;
            double v = origin.Y - sphere.Y;
            double w = origin.Z - sphere.Z;
            // 1/4 дискриминанта.
            double dDiv4 = (direction.X * u + direction.Y * v + direction.Z * w)
                           * (direction.X * u + direction.Y * v + direction.Z * w)
                           - (direction.X * direction.X + direction.Y * direction.Y + direction.Z * direction.Z)
                           * (u * u + v * v + w * w - sphere.radius * sphere.radius);
            // Количество решений квадратного уравнения.
            if (dDiv4 > double.Epsilon)
            {
                // 2 точки пересечения, возвращаем ту, которая ближе.
                double t1 = -(direction.X * u + direction.Y * v + direction.Z * w + Math.Sqrt(dDiv4))
                            / (direction.X * direction.X + direction.Y * direction.Y + direction.Z * direction.Z);
                double t2 = -(direction.X * u + direction.Y * v + direction.Z * w - Math.Sqrt(dDiv4))
                            / (direction.X * direction.X + direction.Y * direction.Y + direction.Z * direction.Z);
                double t;
                if (t1 > 0 && t2 > 0)
                    t = t1 < t2 ? t1 : t2;
                else if (t1 > 0 && t2 <= 0)
                    t = t1;
                else if (t1 <= 0 && t2 > 0)
                    t = t2;
                else
                    return null;
                return new Point3D(origin.X + t * direction.X, origin.Y + t * direction.Y, origin.Z + t * direction.Z);
            }
            else if (dDiv4 > -double.Epsilon)
            {
                // Одна точка пересечения (луч касается сферы).
                double t = -(direction.X * u + direction.Y * v + direction.Z * w)
                           / (direction.X * direction.X + direction.Y * direction.Y + direction.Z * direction.Z);
                if (t < 0)
                    return null;
                return new Point3D(origin.X + t * direction.X, origin.Y + t * direction.Y, origin.Z + t * direction.Z);
            }
            else
                // Нет пересечений.
                return null;
        }

        public static bool PointInFacet(Point3D point, Facet facet)
        {
            // Метод углов.
            double angleSum = 0;
            for (int i = 0; i < facet.points.Count; i++)
            {
                Point3D point1 = facet.points[i];
                Point3D point2 = facet.points[(i + 1) % facet.points.Count];
                point1 = point1.Transform(Transformation.Translate(-point.X, -point.Y, -point.Z));
                point2 = point2.Transform(Transformation.Translate(-point.X, -point.Y, -point.Z));
                double dot = point1.X * point2.X + point1.Y * point2.Y + point1.Z * point2.Z;
                double cross = Vector3D.CrossProduct(
                    new Vector3D(point1.X, point1.Y, point1.Z), new Vector3D(point2.X, point2.Y, point2.Z))
                    .Length;
                double length1 = Magnitude(point1);
                double length2 = Magnitude(point2);
                double cos = dot / (length1 * length2);
                double sin = cross / (length1 * length2);
                double angle = GetAngle(sin, cos);
                angleSum += angle;
            }
            return Math.Abs(Math.Abs(angleSum) - 2 * Math.PI) <= 1e-7;
        }

        public static double Magnitude(Point3D vector)
        {
            return Math.Sqrt(SqrMagnitude(vector));
        }

        public static double SqrMagnitude(Point3D vector)
        {
            return vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z;
        }

        public static double Distance(Point3D point1, Point3D point2)
        {
            return Magnitude(new Point3D(point2.X - point1.X, point2.Y - point1.Y, point2.Z - point1.Z));
        }
        
        public static double SqrDistance(Point3D point1, Point3D point2)
        {
            return SqrMagnitude(new Point3D(point2.X - point1.X, point2.Y - point1.Y, point2.Z - point1.Z));
        }

        public static Vector3D Reflect(Vector3D vector, Vector3D normal)
        {
            normal.Normalize();
            return vector - 2 * Vector3D.DotProduct(vector, normal) * normal;
        }

        public static Vector3D Refract(Vector3D vector, Vector3D normal, double coefficient)
        {
            normal.Normalize();
            double dot = Vector3D.DotProduct(vector, normal);
            if (dot > 0)
            {
                dot = -dot;
                normal = -normal;
            }
            double cos = Math.Sqrt(1 - coefficient * coefficient * (1 - dot * dot));
            return coefficient * vector - (cos + coefficient * dot) * normal;
        }

        public static Vector3D PointToVector(Point3D point)
        {
            return new Vector3D(point.X, point.Y, point.Z);
        }

        public static Point3D VectorToPoint(Vector3D vector)
        {
            return new Point3D(vector.X, vector.Y, vector.Z);
        }
    }
}