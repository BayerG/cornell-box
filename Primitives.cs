﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Editor3D
{
    public interface IPrimitive
    {
        Color Color { get; set; }
        double ReflectionCoefficient { get; set; }
        double RefractiveIndex { get; set; }

        void Apply(Transformation t);
    }

    public class Point3D : IPrimitive
    {
        private double[] coords = { 0, 0, 0, 1 };

        public double X
        {
            get => coords[0];
            set => coords[0] = value;
        }

        public double Y
        {
            get => coords[1];
            set => coords[1] = value;
        }

        public double Z
        {
            get => coords[2];
            set => coords[2] = value;
        }

        public double K
        {
            get => coords[3];
            set => coords[3] = value;
        }

        public Color Color { get; set; } = Color.Black;
        public double ReflectionCoefficient { get; set; } = 0;
        public double RefractiveIndex { get; set; } = 1;
        
        public Point3D(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }
        
        public Point3D(double x, double y, double z, Color color)
        {
            X = x;
            Y = y;
            Z = z;
            this.Color = color;
        }

        public Point3D(Point3D point)
        {
            X = point.X;
            Y = point.Y;
            Z = point.Z;
            Color = point.Color;
        }

        public override string ToString()
        {
            return $"({X}, {Y}, {Z})";
        }

        public void Apply(Transformation t)
        {
            double[] newCoords = new double[4];
            for (int i = 0; i < 4; ++i)
            {
                newCoords[i] = 0;
                for (int j = 0; j < 4; ++j)
                    newCoords[i] += coords[j] * t.matrix[j, i];
            }
            coords = newCoords;
        }

        public Point3D Transform(Transformation t)
        {
            var p = new Point3D(this);
            p.Apply(t);
            return p;
        }

        public void NormalizeToDisplay(int width, int height)
        {
            int minSize = Math.Min(width, height);
            X = width / 2d + X / K / 2 * minSize;
            Y = height / 2d - Y / K / 2 * minSize;
        }

        public Point3D NormalizedToDisplay(int width, int height)
        {
            int minSize = Math.Min(width, height);
            double x = width / 2d + X / K / 2 * minSize;
            double y = height / 2d - Y / K / 2 * minSize;
            return new Point3D(x, y, Z, Color);
        }
    }
    
    public class Line : IPrimitive
    {
        private Point3D a;
        private Point3D b;

        public Point3D A
        {
            get => a;
            set => a = value;
        }
        
        public Point3D B
        {
            get => b;
            set => b = value;
        }

        public Color Color
        {
            get
            {
                if (a.Color == b.Color)
                    return a.Color;
                else
                    return Color.Empty;
            }
            set
            {
                a.Color = value;
                b.Color = value;
            }
        }
        public double ReflectionCoefficient { get; set; } = 0;
        public double RefractiveIndex { get; set; } = 1;

        public Line(Point3D a, Point3D b)
        {
            A = a;
            B = b;
        }
        
        public Line(Point3D a, Point3D b, Color color)
        {
            A = a;
            B = b;
            this.Color = color;
        }

        public void Apply(Transformation t)
        {
            A = A.Transform(t);
            B = B.Transform(t);
        }

        public void Draw(Graphics graphics, Transformation projection, int width, int height)
        {
            Point3D c = A.Transform(projection).NormalizedToDisplay(width, height);
            Point3D d = B.Transform(projection).NormalizedToDisplay(width, height);
            if (c.Z >= 0 && d.Z >= 0)
                graphics.DrawLine(new Pen(Color), (float)c.X, (float)c.Y, (float)d.X, (float)d.Y);
        }
    }
    
    public class Facet : IPrimitive
    {
        public IList<Point3D> points = new List<Point3D>();

        public Color Color { get; set; }
        public double ReflectionCoefficient { get; set; } = 0;
        public double RefractiveIndex { get; set; } = 1;

        public Facet(){}

        public Facet(IList<Point3D> points)
        {
            this.points = points;
        }

        public Facet(Color color)
        {
            this.Color = color;
        }
        
        public Facet(IList<Point3D> points, Color color)
        {
            this.points = points;
            this.Color = color;
        }

        public void Apply(Transformation t)
        {
            foreach (var point in points)
                point.Apply(t);
        }
    }

    public class Light : IPrimitive
    {
        public double intensity;
        
        private double[] coords = { 0, 0, 0, 1 };

        public double X
        {
            get => coords[0];
            set => coords[0] = value;
        }

        public double Y
        {
            get => coords[1];
            set => coords[1] = value;
        }

        public double Z
        {
            get => coords[2];
            set => coords[2] = value;
        }

        public double K
        {
            get => coords[3];
            set => coords[3] = value;
        }

        public Color Color { get; set; } = Color.White;
        public double ReflectionCoefficient { get; set; } = 0;
        public double RefractiveIndex { get; set; } = 1;

        public Light(double x, double y, double z, double intensity)
        {
            X = x;
            Y = y;
            Z = z;
            this.intensity = intensity;
        }
        
        public Light(double x, double y, double z, Color color, double intensity)
        {
            X = x;
            Y = y;
            Z = z;
            this.Color = color;
            this.intensity = intensity;
        }

        public Light(Light light)
        {
            X = light.X;
            Y = light.Y;
            Z = light.Z;
            Color = light.Color;
            this.intensity = light.intensity;
        }

        public void Apply(Transformation t)
        {
            double[] newCoords = new double[4];
            for (int i = 0; i < 4; ++i)
            {
                newCoords[i] = 0;
                for (int j = 0; j < 4; ++j)
                    newCoords[i] += coords[j] * t.matrix[j, i];
            }
            coords = newCoords;
        }

        public Light Transform(Transformation t)
        {
            var p = new Light(this);
            p.Apply(t);
            return p;
        }

        public void NormalizeToDisplay(int width, int height)
        {
            int minSize = Math.Min(width, height);
            X = width / 2d + X / K / 2 * minSize;
            Y = height / 2d - Y / K / 2 * minSize;
        }

        public Light NormalizedToDisplay(int width, int height)
        {
            int minSize = Math.Min(width, height);
            double x = width / 2d + X / K / 2 * minSize;
            double y = height / 2d - Y / K / 2 * minSize;
            return new Light(x, y, Z, Color, intensity);
        }
    }

    public class Sphere : IPrimitive
    {
        public double X
        {
            get => coords[0];
            set => coords[0] = value;
        }

        public double Y
        {
            get => coords[1];
            set => coords[1] = value;
        }

        public double Z
        {
            get => coords[2];
            set => coords[2] = value;
        }

        public double K
        {
            get => coords[3];
            set => coords[3] = value;
        }

        public Color Color { get; set; }
        public double ReflectionCoefficient { get; set; } = 0;
        public double RefractiveIndex { get; set; } = 1;
        
        public double radius = 0;
        
        private double[] coords = { 0, 0, 0, 1 };

        public Sphere(double x, double y, double z, double radius)
        {
            X = x;
            Y = y;
            Z = z;
            this.radius = radius;
        }

        public Sphere(double x, double y, double z, double radius, Color color)
            : this(x, y, z, radius)
        {
            Color = color;
        }

        public Sphere(Sphere sphere)
        {
            X = sphere.X;
            Y = sphere.Y;
            Z = sphere.Z;
            this.radius = sphere.radius;
        }
        
        public void Apply(Transformation t)
        {
            double[] newCoords = new double[4];
            for (int i = 0; i < 4; ++i)
            {
                newCoords[i] = 0;
                for (int j = 0; j < 4; ++j)
                    newCoords[i] += coords[j] * t.matrix[j, i];
            }
            coords = newCoords;
        }
        
        public Sphere Transform(Transformation t)
        {
            Sphere sphere = new Sphere(this);
            sphere.Apply(t);
            return sphere;
        }
    }
}