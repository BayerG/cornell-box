﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Editor3D
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            InitializeScene();
            InitializeCamera();
            InitializeRenderer();
            InitializeInterface();
            Render();
        }

        #region Main Menu

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InitializeScene();
            InitializeCamera();
            InitializeRenderer();
            InitializeInterface();
            Render();
        }
        
        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() != DialogResult.OK)
                return;
            bitmap.Save(saveFileDialog.FileName);
        }
        
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        
        private void tetrahedronToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddTetrahedron();
            Render();
        }

        private void icosahedronToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddIcosahedron();
            Render();
        }

        private void dodecahedronToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddDodecahedron();
            Render();
        }

        private void plotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddPlot();
            Render();
        }

        private void rotationFigureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddRotationFigure();
            Render();
        }

        #endregion

        #region Scene Group Box

        private void primitivesListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            IPrimitive primitive = (IPrimitive) primitivesListBox.SelectedItem;
            if (primitive is Polyhedron || primitive is Sphere)
            {
                SetButtonColorImage(colorButton, primitive.Color);
                reflectionNumericUpDown.Value = (decimal) primitive.ReflectionCoefficient;
                refractionNumericUpDown.Value = (decimal) primitive.RefractiveIndex;
                alphaNumericUpDown.Value = primitive.Color.A;
                removeButton.Enabled = true;
                colorButton.Enabled = true;
                reflectionNumericUpDown.Enabled = true;
                refractionNumericUpDown.Enabled = true;
                alphaNumericUpDown.Enabled = true;
                transformationsGroupBox.Enabled = true;
                centerScaleButton.Enabled = true;
                centerRotateButton.Enabled = true;
            }
            else if (primitive is Light light)
            {
                SetButtonColorImage(colorButton, light.Color);
                removeButton.Enabled = true;
                colorButton.Enabled = true;
                reflectionNumericUpDown.Enabled = false;
                refractionNumericUpDown.Enabled = false;
                alphaNumericUpDown.Enabled = false;
                transformationsGroupBox.Enabled = true;
                centerScaleButton.Enabled = false;
                centerRotateButton.Enabled = false;
            }
            else
            {
                SetButtonColorImage(colorButton, Color.Empty);
                removeButton.Enabled = false;
                colorButton.Enabled = false;
                reflectionNumericUpDown.Enabled = false;
                refractionNumericUpDown.Enabled = false;
                alphaNumericUpDown.Enabled = false;
                transformationsGroupBox.Enabled = false;
                centerScaleButton.Enabled = false;
                centerRotateButton.Enabled = false;
            }
        }
        
        private void tetrahedronButton_Click(object sender, EventArgs e)
        {
            AddTetrahedron();
            Render();
        }

        private void icosahedronButton_Click(object sender, EventArgs e)
        {
            AddIcosahedron();
            Render();
        }

        private void dodecahedronButton_Click(object sender, EventArgs e)
        {
            AddDodecahedron();
            Render();
        }

        private void plotButton_Click(object sender, EventArgs e)
        {
            AddPlot();
            Render();
        }

        private void rotationFigureButton_Click(object sender, EventArgs e)
        {
            AddRotationFigure();
            Render();
        }

        private void lightButton_Click(object sender, EventArgs e)
        {
            AddLight();
            Render();
        }
        
        private void colorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() != DialogResult.OK)
                return;
            ((IPrimitive) primitivesListBox.SelectedItem).Color = colorDialog.Color;
            alphaNumericUpDown.Value = colorDialog.Color.A;
            SetButtonColorImage(colorButton, colorDialog.Color);
            Render();
        }
        
        private void removeButton_Click(object sender, EventArgs e)
        {
            primitivesListBox.Items.Remove(primitivesListBox.SelectedItem);
            Render();
        }
        
        private void clearButton_Click(object sender, EventArgs e)
        {
            InitializeScene();
            InitializeInterface();
            Render();
        }
        
        private void reflectionNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            IPrimitive primitive = (IPrimitive) primitivesListBox.SelectedItem;
            primitive.ReflectionCoefficient = (double) reflectionNumericUpDown.Value;
            Render();
        }
        
        private void refractionNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            IPrimitive primitive = (IPrimitive) primitivesListBox.SelectedItem;
            primitive.RefractiveIndex = (double) refractionNumericUpDown.Value;
            Render();
        }

        private void alphaNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            IPrimitive primitive = (IPrimitive) primitivesListBox.SelectedItem;
            primitive.Color = Color.FromArgb((int) (alphaNumericUpDown.Value),
                primitive.Color.R, primitive.Color.G, primitive.Color.B);
            Render();
        }
        
        private void mirrorWallNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            InitializeScene();
            Render();
        }
        
        #endregion

        #region Transformations Group Box

        private void translateButton_Click(object sender, EventArgs e)
        {
            Translate();
            Render();
        }

        private void rotateButton_Click(object sender, EventArgs e)
        {
            Rotate();
            Render();
        }

        private void scaleButton_Click(object sender, EventArgs e)
        {
            Scale();
            Render();
        }

        private void reflectButton_Click(object sender, EventArgs e)
        {
            Reflect();
            Render();
        }

        private void centerScaleButton_Click(object sender, EventArgs e)
        {
            CenterScale();
            Render();
        }

        private void centerRotateButton_Click(object sender, EventArgs e)
        {
            CenterRotate();
            Render();
        }

        private void lineRotateButton_Click(object sender, EventArgs e)
        {
            LineRotate();
            Render();
        }

        #endregion

        #region Camera Group Box

        private void cameraPositionXNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (lockCameraEventHandlers)
                return;
            SetCameraPosition();
            Render();
        }

        private void cameraPositionYNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (lockCameraEventHandlers)
                return;
            SetCameraPosition();
            Render();
        }

        private void cameraPositionZNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (lockCameraEventHandlers)
                return;
            SetCameraPosition();
            Render();
        }

        private void cameraRotationXNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (lockCameraEventHandlers)
                return;
            SetCameraRotation();
            Render();
        }

        private void cameraRotationYNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (lockCameraEventHandlers)
                return;
            SetCameraRotation();
            Render();
        }

        private void cameraRotationZNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (lockCameraEventHandlers)
                return;
            SetCameraRotation();
            Render();
        }
        
        private void cameraDirectionXNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (lockCameraEventHandlers)
                return;
            SetCameraDirectionRoll();
            Render();
        }

        private void cameraDirectionYNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (lockCameraEventHandlers)
                return;
            SetCameraDirectionRoll();
            Render();
        }

        private void cameraDirectionZNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (lockCameraEventHandlers)
                return;
            SetCameraDirectionRoll();
            Render();
        }

        private void cameraPointRotateButton_Click(object sender, EventArgs e)
        {
            PointRotateCamera();
            Render();
        }
        
        private void fovNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            camera.fieldOfView = (double) ((NumericUpDown) sender).Value;
            Render();
        }
        
        private void cameraRollNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (lockCameraEventHandlers)
                return;
            SetCameraDirectionRoll();
            Render();
        }
        
        #endregion

        #region Rendering Group Box
        
        private void renderModeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRenderMode();
            Render();
        }
        
        private void ambientColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() != DialogResult.OK)
                return;
            ambientColor = colorDialog.Color;
            SetButtonColorImage(ambientColorButton, colorDialog.Color);
            SetRenderMode();
            Render();
        }
        
        private void boundaryColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() != DialogResult.OK)
                return;
            boundaryColor = colorDialog.Color;
            SetButtonColorImage(boundaryColorButton, colorDialog.Color);
            SetRenderMode();
            Render();
        }

        private void visibleOnlyCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            SetRenderMode();
            Render();
        }
        
        private void renderingDepthNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            SetRenderMode();
            Render();
        }
        
        #endregion
    }
}