﻿using System;
using System.Drawing;
using System.Collections.Generic;

namespace Editor3D
{
    public partial class Polyhedron
    {
        public static Polyhedron Dodecahedron(double size)
        {
            return Dodecahedron(size, Color.Black);
        }

        public static Polyhedron Dodecahedron(double size, Color color)
        {
            void InitFacets(Polyhedron p)
            {
                // 12 граней
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[10], p.points[12],
                    p.points[14], p.points[16], p.points[18]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[11], p.points[13],
                    p.points[15], p.points[17], p.points[19]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[0], p.points[1],
                    p.points[2], p.points[12], p.points[10]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[2], p.points[3],
                    p.points[4], p.points[14], p.points[12]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[4], p.points[5],
                    p.points[6], p.points[16], p.points[14]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[6], p.points[7],
                    p.points[8], p.points[18], p.points[16]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[8], p.points[9],
                    p.points[0], p.points[10], p.points[18]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[1], p.points[2],
                    p.points[3], p.points[13], p.points[11]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[3], p.points[4],
                    p.points[5], p.points[15], p.points[13]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[5], p.points[6],
                    p.points[7], p.points[17], p.points[15]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[7], p.points[8],
                    p.points[9], p.points[19], p.points[17]
                }, color));
                p.facets.Add(new Facet(new Point3D[]
                {
                    p.points[9], p.points[0],
                    p.points[1], p.points[11], p.points[19]
                }, color));
            }

            Polyhedron icosahedron = Polyhedron.Icosahedron(size);
            Polyhedron polyhedron = new Polyhedron("Dodecahedron");

            for (int i = 0; i < 20; i++)
            {
                double x = (icosahedron.facets[i].points[0].X
                            + icosahedron.facets[i].points[1].X
                            + icosahedron.facets[i].points[2].X) / 3;
                double y = (icosahedron.facets[i].points[0].Y
                            + icosahedron.facets[i].points[1].Y
                            + icosahedron.facets[i].points[2].Y) / 3;
                double z = (icosahedron.facets[i].points[0].Z
                            + icosahedron.facets[i].points[1].Z
                            + icosahedron.facets[i].points[2].Z) / 3;
                polyhedron.points.Add(new Point3D(x, y, z, color));
            }
            InitFacets(polyhedron);

            return polyhedron;
        }
    }
}