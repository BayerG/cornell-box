﻿using System;
using System.Drawing;
using System.Collections.Generic;

namespace Editor3D
{
    public partial class Polyhedron
    {
        public static Polyhedron RotationFigure(List<Point3D> generatrix, Transformation matrix, int splits)
        {
            return Polyhedron.RotationFigure(generatrix, matrix, splits, Color.Black);
        }

        public static Polyhedron RotationFigure(List<Point3D> generatrix, Transformation matrix, int splits,
            Color color)
        {
            Polyhedron polyhedron = new Polyhedron("Rotation Figure");

            double angle = (360d / splits) * Math.PI / 180;
            List<Point3D> currentList = new List<Point3D>();
            Facet topFacet = new Facet(color);
            Facet downFacet = new Facet(color);

            for (int i = 0; i < generatrix.Count; i++)
            {
                currentList.Add(new Point3D(generatrix[i]));
                polyhedron.points.Add(new Point3D(generatrix[i]));
            }
            topFacet.points.Add(polyhedron.points[0]);
            downFacet.points.Add(polyhedron.points[polyhedron.points.Count - 1]);

            // Добавление всех граней кроме последнего слоя
            for (int i = 0; i < splits - 1; i++)
            {
                currentList[0].Apply(matrix);
                polyhedron.points.Add(new Point3D(currentList[0]));
                topFacet.points.Add(polyhedron.points[polyhedron.points.Count - 1]);
                for (int j = 1; j < currentList.Count; j++)
                {
                    currentList[j].Apply(matrix);
                    polyhedron.points.Add(new Point3D(currentList[j]));
                    Facet facet = new Facet(
                        new Point3D[]
                        {
                            polyhedron.points[polyhedron.points.Count - generatrix.Count - 2],
                            polyhedron.points[polyhedron.points.Count - generatrix.Count - 1],
                            polyhedron.points[polyhedron.points.Count - 1],
                            polyhedron.points[polyhedron.points.Count - 2]
                        }, color);
                    polyhedron.facets.Add(facet);
                }
                downFacet.points.Add(polyhedron.points[polyhedron.points.Count - 1]);
            }

            // Добавление последного слоя граней
            for (int j = 1; j < generatrix.Count; j++)
            {
                Facet facet = new Facet(
                    new Point3D[]
                    {
                        polyhedron.points[polyhedron.points.Count - generatrix.Count + j - 1],
                        polyhedron.points[polyhedron.points.Count - generatrix.Count + j],
                        polyhedron.points[j],
                        polyhedron.points[j - 1]
                    }, color);
                polyhedron.facets.Add(facet);
            }
            polyhedron.facets.Add(topFacet);
            polyhedron.facets.Add(downFacet);

            return polyhedron;
        }
    }
}