﻿using System.Drawing;
using org.mariuszgromada.math.mxparser;

namespace Editor3D
{
    public partial class Polyhedron
    {
        public static Polyhedron Plot(string function, double minX, double maxX, double minZ, double maxZ,
            int xTicks, int zTicks)
        {
            return Polyhedron.Plot(function, minX, maxX, minZ, maxZ, xTicks, zTicks, Color.Black);
        }
        
        public static Polyhedron Plot(string function, double minX, double maxX, double minZ, double maxZ,
            int xTicks, int zTicks, Color color)
        {
            Polyhedron polyhedron = new Polyhedron("Plot");

            Argument xArgument = new Argument("x");
            Argument zArgument = new Argument("z");
            Expression expression = new Expression(function, xArgument, zArgument);

            double deltaX = (maxX - minX) / xTicks;
            double deltaY = (maxZ - minZ) / zTicks;

            // Добавляем точки.
            double z = minZ;
            for (int i = 0; i <= zTicks; i++)
            {
                double x = minX;
                for (int j = 0; j <= xTicks; j++)
                {
                    xArgument.setArgumentValue(x);
                    zArgument.setArgumentValue(z);
                    double y = expression.calculate();
                    polyhedron.points.Add(new Point3D(x, y, z, color));

                    x += deltaX;
                }
                z += deltaY;
            }

            // Добавляем грани.
            for (int i = 0; i < zTicks; i++)
                for (int j = 0; j < xTicks; j++)
                    polyhedron.facets.Add(new Facet(new Point3D[]
                    {
                        polyhedron.points[i * (zTicks + 1) + j],
                        polyhedron.points[i * (zTicks + 1) + (j + 1)],
                        polyhedron.points[(i + 1) * (zTicks + 1) + (j + 1)],
                        polyhedron.points[(i + 1) * (zTicks + 1) + j]
                    }, color));

            return polyhedron;
        }
    }
}