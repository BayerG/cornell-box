﻿using System;
using System.Drawing;
using System.Collections.Generic;

namespace Editor3D
{
    public partial class Polyhedron
    {
        public static Polyhedron Tetrahedron(double size)
        {
            return Tetrahedron(size, Color.Black);
        }

        public static Polyhedron Tetrahedron(double size, Color color)
        {
            Polyhedron polyhedron = new Polyhedron("Tetrahedron");

            double h = Math.Sqrt(2.0 / 3.0) * size;
            polyhedron.points = new List<Point3D>();

            polyhedron.points.Add(new Point3D(-size / 2, 0, h / 3, color));
            polyhedron.points.Add(new Point3D(0, 0, -h * 2 / 3, color));
            polyhedron.points.Add(new Point3D(size / 2, 0, h / 3, color));
            polyhedron.points.Add(new Point3D(0, h, 0, color));

            // Основание тетраэдра
            polyhedron.facets.Add(new Facet(new Point3D[]
            {
                polyhedron.points[0], polyhedron.points[1], polyhedron.points[2]
            }, color));
            // Левая грань
            polyhedron.facets.Add(new Facet(new Point3D[]
            {
                polyhedron.points[1], polyhedron.points[3], polyhedron.points[0]
            }, color));
            // Правая грань
            polyhedron.facets.Add(new Facet(new Point3D[]
            {
                polyhedron.points[2], polyhedron.points[3], polyhedron.points[1]
            }, color));
            // Передняя грань
            polyhedron.facets.Add(new Facet(new Point3D[]
            {
                polyhedron.points[0], polyhedron.points[3], polyhedron.points[2]
            }, color));

            return polyhedron;
        }
    }
}