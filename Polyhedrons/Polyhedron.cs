﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using org.mariuszgromada.math.mxparser;

namespace Editor3D
{
    public partial class Polyhedron : IPrimitive
    {
        public List<Point3D> points = new List<Point3D>();
        public List<Facet> facets = new List<Facet>();

        public Point3D Center
        {
            get
            {
                if (points.Count == 0)
                    return null;
                double averageX = points.Average(point => point.X);
                double averageY = points.Average(point => point.Y);
                double averageZ = points.Average(point => point.Z);
                return new Point3D(averageX, averageY, averageZ);
            }
        }

        public string name;

        public Color Color
        {
            get
            {
                Color currentColor = facets[0].Color;
                foreach (Facet facet in facets)
                    if (facet.Color != currentColor)
                        return Color.Empty;
                return currentColor;
            }
            set
            {
                foreach (Facet facet in facets)
                    facet.Color = value;
            }
        }
        
        public double ReflectionCoefficient
        {
            get
            {
                double currentCoefficient = facets[0].ReflectionCoefficient;
                foreach (Facet facet in facets)
                    if (Math.Abs(facet.ReflectionCoefficient - currentCoefficient) > double.Epsilon)
                        return 0;
                return currentCoefficient;
            }
            set
            {
                foreach (Facet facet in facets)
                    facet.ReflectionCoefficient = value;
            }
        }

        public double RefractiveIndex
        {
            get
            {
                double currentIndex = facets[0].RefractiveIndex;
                foreach (Facet facet in facets)
                    if (Math.Abs(facet.RefractiveIndex - currentIndex) > double.Epsilon)
                        return 0;
                return currentIndex;
            }
            set
            {
                foreach (Facet facet in facets)
                    facet.RefractiveIndex = value;
            }
        }

        public Polyhedron(string name = "Polyhedron")
        {
            this.name = name;
        }

        public Polyhedron(List<Point3D> points, List<Facet> facets, string name = "Polyhedron")
        {
            this.points = points;
            this.facets = facets;
            this.name = name;
        }

        public void Apply(Transformation t)
        {
            foreach (var point in points)
                point.Apply(t);
        }

        public Polyhedron Copy()
        {
            Polyhedron copiedPolyhedron = new Polyhedron(new List<Point3D>(),
                facets.Select(facet => new Facet(facet.points.ToList())).ToList(), name);
            foreach (Point3D point in points)
            {
                Point3D copiedPoint = new Point3D(point.X, point.Y, point.Z, point.Color);
                foreach (Facet facet in copiedPolyhedron.facets)
                    if (facet.points.Contains(point))
                        facet.points[facet.points.IndexOf(point)] = copiedPoint;
                copiedPolyhedron.points.Add(copiedPoint);
            }
            return copiedPolyhedron;
        }
    }
}