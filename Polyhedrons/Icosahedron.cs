﻿using System;
using System.Drawing;
using System.Collections.Generic;

namespace Editor3D
{
    public partial class Polyhedron
    {
        public static Polyhedron Icosahedron(double size)
        {
            return Icosahedron(size, Color.Black);
        }

        public static Polyhedron Icosahedron(double size, Color color)
        {
            Polyhedron polyhedron = new Polyhedron("Icosahedron");

            // радиус описанной сферы
            double R = (size * Math.Sqrt(2.0 * (5.0 + Math.Sqrt(5.0)))) / 4;

            // радиус вписанной сферы
            double r = (size * Math.Sqrt(3.0) * (3.0 + Math.Sqrt(5.0))) / 12;

            polyhedron.points = new List<Point3D>();

            for (int i = 0; i < 5; ++i)
            {
                polyhedron.points.Add(new Point3D(
                    r * Math.Cos(2 * Math.PI / 5 * i),
                    R / 2,
                    r * Math.Sin(2 * Math.PI / 5 * i), color));
                polyhedron.points.Add(new Point3D(
                    r * Math.Cos(2 * Math.PI / 5 * i + 2 * Math.PI / 10),
                    -R / 2,
                    r * Math.Sin(2 * Math.PI / 5 * i + 2 * Math.PI / 10), color));
            }

            polyhedron.points.Add(new Point3D(0, R, 0, color));
            polyhedron.points.Add(new Point3D(0, -R, 0, color));

            // середина
            for (int i = 0; i < 10; ++i)
                polyhedron.facets.Add(new Facet(
                    new Point3D[]
                        {polyhedron.points[i], polyhedron.points[(i + 1) % 10], polyhedron.points[(i + 2) % 10]},
                    color));

            for (int i = 0; i < 5; ++i)
            {
                // верхняя часть
                polyhedron.facets.Add(new Facet(
                    new Point3D[]
                        {polyhedron.points[2 * i], polyhedron.points[10], polyhedron.points[(2 * (i + 1)) % 10]},
                    color));
                // нижняя часть
                polyhedron.facets.Add(new Facet(
                    new Point3D[]
                    {
                        polyhedron.points[2 * i + 1], polyhedron.points[11], polyhedron.points[(2 * (i + 1) + 1) % 10]
                    },
                    color));
            }

            return polyhedron;
        }
    }
}