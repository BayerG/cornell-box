﻿using System;
using System.Drawing;
using System.Collections.Generic;

namespace Editor3D
{
    public partial class Polyhedron
    {
        public static Polyhedron Sphere(double size, int horizontalSplits, int verticalSplits)
        {
            return Sphere(size, horizontalSplits, verticalSplits, Color.Black);
        }

        public static Polyhedron Sphere(double size, int horizontalSplits, int verticalSplits, Color color)
        {
            List<Point3D> generatrix = new List<Point3D>();
            double zAngleDelta = Math.PI / verticalSplits;
            double zAngle = -Math.PI / 2;
            for (int i = 0; i < verticalSplits + 1; i++)
            {
                double x = size * Math.Cos(zAngle);
                double y = size * Math.Sin(zAngle);
                generatrix.Add(new Point3D(x, y, 0, color));
                zAngle += zAngleDelta;
            }
            double yAngle = 2 * Math.PI / horizontalSplits;
            Transformation matrix = Transformation.RotateY(yAngle);
            return Polyhedron.RotationFigure(generatrix, matrix, horizontalSplits, color);
        }
    }
}