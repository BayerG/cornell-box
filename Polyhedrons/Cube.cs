﻿using System.Drawing;

namespace Editor3D
{
    public partial class Polyhedron
    {
        public static Polyhedron Cube(double size)
        {
            return Cube(size, Color.Black);
        }

        public static Polyhedron Cube(double size, Color color)
        {
            Polyhedron polyhedron = new Polyhedron("Cube");

            for (double z = -size / 2; z < size; z += size)
            {
                polyhedron.points.Add(new Point3D(-size / 2, -size / 2, z, color));
                polyhedron.points.Add(new Point3D(-size / 2, size / 2, z, color));
                polyhedron.points.Add(new Point3D(size / 2, size / 2, z, color));
                polyhedron.points.Add(new Point3D(size / 2, -size / 2, z, color));
            }

            polyhedron.facets.Add(new Facet(new Point3D[]
                {polyhedron.points[3], polyhedron.points[2], polyhedron.points[1], polyhedron.points[0]}, color));
            polyhedron.facets.Add(new Facet(new Point3D[]
                {polyhedron.points[4], polyhedron.points[5], polyhedron.points[6], polyhedron.points[7]}, color));
            polyhedron.facets.Add(new Facet(new Point3D[]
                {polyhedron.points[0], polyhedron.points[1], polyhedron.points[5], polyhedron.points[4]}, color));
            polyhedron.facets.Add(new Facet(new Point3D[]
                {polyhedron.points[2], polyhedron.points[3], polyhedron.points[7], polyhedron.points[6]}, color));
            polyhedron.facets.Add(new Facet(new Point3D[]
                {polyhedron.points[0], polyhedron.points[4], polyhedron.points[7], polyhedron.points[3]}, color));
            polyhedron.facets.Add(new Facet(new Point3D[]
                {polyhedron.points[1], polyhedron.points[2], polyhedron.points[6], polyhedron.points[5]}, color));

            return polyhedron;
        }
    }
}