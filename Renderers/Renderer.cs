﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace Editor3D
{
    public abstract class Renderer
    {
        public IEnumerable<IPrimitive> primitives;
        public Camera camera;
        public Bitmap bitmap;
        public Color ambientColor;
        
        public Renderer(IEnumerable<IPrimitive> primitives, Camera camera, Bitmap bitmap, Color ambientColor)
        {
            this.primitives = primitives;
            this.camera = camera;
            this.bitmap = bitmap;
            this.ambientColor = ambientColor;
        }

        public abstract IEnumerable<double> Render();
        
        protected virtual unsafe void SetPixel(BitmapData bitmapData, int x, int y, Color color)
        {
            if (x < 0 || x >= bitmapData.Width || y < 0 || y >= bitmapData.Height)
                return;
            byte* bitmapPointer = (byte*)bitmapData.Scan0.ToPointer();
            bitmapPointer[bitmapData.Stride * y + 3 * x] = color.B;
            bitmapPointer[bitmapData.Stride * y + 3 * x + 1] = color.G;
            bitmapPointer[bitmapData.Stride * y + 3 * x + 2] = color.R;
        }
    }
}