﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Media.Media3D;

namespace Editor3D
{
    public class RayTracingRenderer : Renderer
    {
        public int depth;

        public RayTracingRenderer(IEnumerable<IPrimitive> primitives, Camera camera, Bitmap bitmap, Color ambientColor,
            int depth)
            : base(primitives, camera, bitmap, ambientColor)
        {
            this.depth = depth;
        }

        public override IEnumerable<double> Render()
        {
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            for (int y = 0; y < bitmapData.Height; y++)
                for (int x = 0; x < bitmapData.Width; x++)
                {
                    double normalizedX = (double) x / bitmapData.Width - 0.5;
                    double normalizedY = (double) y / bitmapData.Height - 0.5;
                    Point3D origin = new Point3D(normalizedX, normalizedY, 0).Transform(
                        Transformation.RotateZ(camera.rotation.Z)
                        * Transformation.RotateX(camera.rotation.X)
                        * Transformation.RotateY(camera.rotation.Y)
                        * Transformation.Translate(camera.position.X, camera.position.Y, camera.position.Z));
                    Point3D direction
                        = new Point3D(normalizedX * camera.fieldOfView, normalizedY * camera.fieldOfView, 1).Transform(
                            Transformation.RotateX(camera.rotation.X)
                            * Transformation.RotateY(camera.rotation.Y));
                    Color color = Trace(origin, direction, out _, depth);
                    SetPixel(bitmapData, x, bitmap.Height - 1 - y, color);
                    yield return (double) (y * bitmapData.Width + x) / (bitmapData.Width * bitmapData.Height);
                }
            bitmap.UnlockBits(bitmapData);
            yield return 1;
        }
        
        protected virtual Color Trace(Point3D origin, Point3D direction, out Tuple<Point3D, IPrimitive> hit, int depth,
            double currentRefractiveIndex = 1)
        {
            Color color = ambientColor;
            hit = null;
            if (depth == 0)
                return color;

            origin = new Point3D(
                origin.X + direction.X * 1e-3,
                origin.Y + direction.Y * 1e-3,
                origin.Z + direction.Z * 1e-3);

            double intensityR = 0;
            double intensityG = 0;
            double intensityB = 0;
            
            List<Tuple<Point3D, IPrimitive>> intersections = new List<Tuple<Point3D, IPrimitive>>();
            foreach (IPrimitive primitive in primitives)
                if (primitive is Polyhedron polyhedron)
                    foreach (Facet facet in polyhedron.facets)
                    {
                        Point3D intersectionPoint = Geometry.FacetIntersection(origin, direction, facet, polyhedron);
                        if (intersectionPoint != null)
                            intersections.Add(
                                new Tuple<Point3D, IPrimitive>(intersectionPoint, facet));
                    }
                else if (primitive is Sphere sphere)
                {
                    Point3D intersectionPoint = Geometry.SphereIntersection(origin, direction, sphere);
                    if (intersectionPoint != null)
                        intersections.Add(
                            new Tuple<Point3D, IPrimitive>(intersectionPoint, sphere));
                }
            
            if (intersections.Count != 0)
            {
                Tuple<Point3D, IPrimitive> intersection
                    = intersections.Aggregate((closestIntersection, currentIntersection)
                    => Geometry.SqrDistance(origin, currentIntersection.Item1)
                       < Geometry.SqrDistance(origin, closestIntersection.Item1)
                        ? currentIntersection
                        : closestIntersection);
                hit = new Tuple<Point3D, IPrimitive>(intersection.Item1, intersection.Item2);

                Vector3D viewVector = new Vector3D(
                    intersection.Item1.X - origin.X,
                    intersection.Item1.Y - origin.Y,
                    intersection.Item1.Z - origin.Z);
                Vector3D normalVector = new Vector3D();
                if (intersection.Item2 is Facet facet)
                    normalVector = Geometry.FacetNormal(facet);
                else if (intersection.Item2 is Sphere sphere)
                    normalVector = Geometry.SphereNormal(intersection.Item1, sphere);
                
                // Локальная модель освещения.
                foreach (IPrimitive primitive in primitives)
                    if (primitive is Light light)
                    {
                        Vector3D lightVector = new Vector3D(
                            light.X - intersection.Item1.X,
                            light.Y - intersection.Item1.Y,
                            light.Z - intersection.Item1.Z);
                        double viewNormalDot = Vector3D.DotProduct(viewVector, normalVector);
                        double lightNormalDot = Vector3D.DotProduct(lightVector, normalVector);
                        if (viewNormalDot * lightNormalDot > 0)
                            continue;

                        double cos = lightNormalDot / (lightVector.Length * normalVector.Length);
                        if (cos < 0)
                            cos = -cos;

                        Color shadowColor = light.Color;
                        Point3D shadowOrigin = intersection.Item1;
                        while (true)
                        {
                            Trace(shadowOrigin, Geometry.VectorToPoint(lightVector),
                                out Tuple<Point3D, IPrimitive> shadowHit, depth - 1);
                            if (shadowHit == null
                                || Geometry.SqrDistance(intersection.Item1, shadowHit.Item1)
                                >= Geometry.SqrDistance(intersection.Item1, new Point3D(light.X, light.Y, light.Z)))
                                break;
                            else if (shadowHit.Item2.Color.A == 255)
                            {
                                shadowColor = Color.Black;
                                break;
                            }
                            else
                            {
                                double transparency = (255 - shadowHit.Item2.Color.A) / 255d;
                                shadowColor = Color.FromArgb(
                                    (int) (light.Color.R * transparency),
                                    (int) (light.Color.G * transparency),
                                    (int) (light.Color.B * transparency));
                                shadowOrigin = shadowHit.Item1;
                            }
                        }
                        
                        intensityR += shadowColor.R * light.intensity * cos / 255 * intersection.Item2.Color.R / 255;
                        intensityG += shadowColor.G * light.intensity * cos / 255 * intersection.Item2.Color.G / 255;
                        intensityB += shadowColor.B * light.intensity * cos / 255 * intersection.Item2.Color.B / 255;
                    }

                // Отражение.
                if (Math.Abs(intersection.Item2.ReflectionCoefficient) > double.Epsilon)
                {
                    Vector3D reflectionVector = Geometry.Reflect(viewVector, normalVector);
                    Color reflectionColor = Trace(intersection.Item1, Geometry.VectorToPoint(reflectionVector),
                        out _, depth - 1);
                    intensityR += reflectionColor.R * intersection.Item2.ReflectionCoefficient / 255;
                    intensityG += reflectionColor.G * intersection.Item2.ReflectionCoefficient / 255;
                    intensityB += reflectionColor.B * intersection.Item2.ReflectionCoefficient / 255;
                }
                
                // Прозрачность.
                if (intersection.Item2.Color.A != 255)
                {
                    double transparency = (255 - intersection.Item2.Color.A) / 255d;
                    Vector3D refractionVector = Geometry.Refract(viewVector, normalVector,
                        currentRefractiveIndex / intersection.Item2.RefractiveIndex);
                    Color refractionColor = Trace(intersection.Item1, Geometry.VectorToPoint(refractionVector),
                        out _, depth - 1, intersection.Item2.RefractiveIndex);
                    intensityR += refractionColor.R * transparency / 255 * intersection.Item2.Color.R / 255;
                    intensityG += refractionColor.G * transparency / 255 * intersection.Item2.Color.G / 255;
                    intensityB += refractionColor.B * transparency / 255 * intersection.Item2.Color.B / 255;
                }

                color = Color.FromArgb(
                    Math.Max(ambientColor.R, Math.Min((int) (255 * intensityR), 255)),
                    Math.Max(ambientColor.G, Math.Min((int) (255 * intensityG), 255)),
                    Math.Max(ambientColor.B, Math.Min((int) (255 * intensityB), 255)));

            }

            return color;
        }
    }
}