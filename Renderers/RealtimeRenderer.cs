﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace Editor3D
{
    public abstract class RealtimeRenderer : Renderer
    {
        public RealtimeRenderer(IEnumerable<IPrimitive> primitives, Camera camera, Bitmap bitmap, Color ambientColor)
            : base(primitives, camera, bitmap, ambientColor) { }
        
        public override IEnumerable<double> Render()
        {
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            List<IPrimitive> transformedPrimitives = Transform(primitives, camera);
            List<List<Point2D>> rasterizedPrimitives = Rasterize(transformedPrimitives);
            Draw(rasterizedPrimitives, bitmapData);
            bitmap.UnlockBits(bitmapData);
            yield return 1;
        }
        
        protected class Point2D
        {
            public int x;
            public int y;
            public double z;
            public Color color;

            public Point2D(int x, int y, double z, Color color)
            {
                this.x = x;
                this.y = y;
                this.z = z;
                this.color = color;
            }

            public static explicit operator Point2D(Point3D point)
                => new Point2D((int) point.X, (int) point.Y, point.Z, point.Color);
        }

        protected virtual List<IPrimitive> Transform(IEnumerable<IPrimitive> primitives, Camera camera)
        {
            List<IPrimitive> transformedPrimitives = new List<IPrimitive>();
            foreach (IPrimitive primitive in primitives)
                if (primitive is Point3D point)
                    transformedPrimitives.Add(point
                        .Transform(camera.Projection)
                        .NormalizedToDisplay(bitmap.Width, bitmap.Height));
                else if (primitive is Light light)
                    transformedPrimitives.Add(light
                        .Transform(camera.Projection)
                        .NormalizedToDisplay(bitmap.Width, bitmap.Height));
                else if (primitive is Line line)
                    transformedPrimitives.Add(new Line(
                        line.A
                            .Transform(camera.Projection)
                            .NormalizedToDisplay(bitmap.Width, bitmap.Height),
                        line.B
                            .Transform(camera.Projection)
                            .NormalizedToDisplay(bitmap.Width, bitmap.Height)));
                else if (primitive is Facet facet)
                    transformedPrimitives.Add(new Facet(
                        facet.points.Select(
                            facetPoint => facetPoint
                                .Transform(camera.Projection)
                                .NormalizedToDisplay(bitmap.Width, bitmap.Height)).ToList()));
                else if (primitive is Polyhedron polyhedron)
                {
                    Polyhedron transformedPolyhedron = polyhedron.Copy();
                    foreach (Point3D polyhedronPoint in transformedPolyhedron.points)
                    {
                        polyhedronPoint.Apply(camera.Projection);
                        polyhedronPoint.NormalizeToDisplay(bitmap.Width, bitmap.Height);
                    }
                    transformedPrimitives.Add(transformedPolyhedron);
                }
            return transformedPrimitives;
        }

        protected virtual List<List<Point2D>> Rasterize(List<IPrimitive> transformedPrimitives)
        {
            List<List<Point2D>> rasterizedPrimitives = new List<List<Point2D>>();
            foreach (IPrimitive primitive in transformedPrimitives)
                if (primitive is Point3D point)
                    rasterizedPrimitives.Add(new List<Point2D>(new[] {(Point2D) point}));
                else if (primitive is Line line)
                    rasterizedPrimitives.Add(RasterizeLine(line));
                else if (primitive is Facet facet)
                    rasterizedPrimitives.Add(RasterizeFacet(facet));
                else if (primitive is Polyhedron polyhedron)
                    rasterizedPrimitives.Add(RasterizePolyhedron(polyhedron));
                else if (primitive is Light light)
                    rasterizedPrimitives.Add(RasterizeLight(light));
            return rasterizedPrimitives;
        }

        protected virtual void Draw(List<List<Point2D>> rasterizedPrimitives, BitmapData bitmapData)
        {
            Clear(bitmapData);
            foreach (List<Point2D> pointList in rasterizedPrimitives)
                foreach (Point2D point in pointList)
                    if (0 <= point.x && point.x < bitmapData.Width
                        && 0 <= point.y && point.y < bitmapData.Height
                        && point.z >= 0)
                        SetPixel(bitmapData, point.x, point.y, point.color);
        }

        protected abstract List<Point2D> RasterizeLine(Line line);

        protected abstract List<Point2D> RasterizeFacet(Facet facet);

        protected abstract List<Point2D> RasterizePolyhedron(Polyhedron transformedPolyhedron);
        
        protected abstract List<Point2D> RasterizeLight(Light light);

        protected virtual void Clear(BitmapData bitmapData)
        {
            for (int y = 0; y < bitmapData.Height; y++)
                for (int x = 0; x < bitmapData.Width; x++)
                    SetPixel(bitmapData, x, y, ambientColor);
        }
    }
}