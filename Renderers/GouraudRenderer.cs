﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Media.Media3D;

namespace Editor3D
{
    public class GouraudRenderer : SolidRenderer
    {
        public GouraudRenderer(IEnumerable<IPrimitive> primitives, Camera camera, Bitmap bitmap, Color ambientColor,
            bool visibleOnly, Color boundaryColor)
            : base(primitives, camera, bitmap, ambientColor, visibleOnly, boundaryColor) { }

        protected override List<IPrimitive> Transform(IEnumerable<IPrimitive> primitives, Camera camera)
        {
            List<IPrimitive> transformedPrimitives = new List<IPrimitive>();
            foreach (IPrimitive primitive in primitives)
                if (primitive is Point3D point)
                    transformedPrimitives.Add(point
                        .Transform(camera.Projection)
                        .NormalizedToDisplay(bitmap.Width, bitmap.Height));
                else if (primitive is Light light)
                    transformedPrimitives.Add(light
                        .Transform(camera.Projection)
                        .NormalizedToDisplay(bitmap.Width, bitmap.Height));
                else if (primitive is Line line)
                    transformedPrimitives.Add(new Line(
                        line.A
                            .Transform(camera.Projection)
                            .NormalizedToDisplay(bitmap.Width, bitmap.Height),
                        line.B
                            .Transform(camera.Projection)
                            .NormalizedToDisplay(bitmap.Width, bitmap.Height)));
                else if (primitive is Facet facet)
                    transformedPrimitives.Add(new Facet(
                        facet.points.Select(
                            facetPoint => facetPoint
                                .Transform(camera.Projection)
                                .NormalizedToDisplay(bitmap.Width, bitmap.Height)).ToList()));
                else if (primitive is Polyhedron polyhedron)
                {
                    Polyhedron transformedPolyhedron = polyhedron.Copy();
                    foreach (Point3D polyhedronPoint in transformedPolyhedron.points)
                    {
                        List<Vector3D> facetsNormals = new List<Vector3D>();
                        foreach (Facet polyhedronFacet in transformedPolyhedron.facets)
                            if (polyhedronFacet.points.Contains(polyhedronPoint))
                                facetsNormals.Add(Geometry.FacetNormal(polyhedronFacet, transformedPolyhedron));
                        Vector3D pointNormal = new Vector3D(
                            facetsNormals.Average(normal => normal.X),
                            facetsNormals.Average(normal => normal.Y),
                            facetsNormals.Average(normal => normal.Z));
                        if (polyhedron.name == "Rotation Figure")
                            pointNormal = -pointNormal;
                        double intensityR = 0;
                        double intensityG = 0;
                        double intensityB = 0;
                        foreach (IPrimitive primitive1 in primitives)
                            if (primitive1 is Light light1)
                            {
                                Vector3D lightVector = new Vector3D(light1.X - polyhedronPoint.X,
                                    light1.Y - polyhedronPoint.Y,
                                    light1.Z - polyhedronPoint.Z);
                                double cos = Vector3D.DotProduct(pointNormal, lightVector)
                                             / (pointNormal.Length * lightVector.Length);
                                if (cos >= 0)
                                {
                                    intensityR += light1.intensity * (light1.Color.R / 255d) * cos;
                                    intensityG += light1.intensity * (light1.Color.G / 255d) * cos;
                                    intensityB += light1.intensity * (light1.Color.B / 255d) * cos;
                                }
                            }
                        Color newColor = Color.FromArgb(
                            Math.Min((int) (polyhedronPoint.Color.R * intensityR), 255),
                            Math.Min((int) (polyhedronPoint.Color.G * intensityG), 255),
                            Math.Min((int) (polyhedronPoint.Color.B * intensityB), 255));
                        if (polyhedron.name == "Plot")
                        {
                            pointNormal = -pointNormal;
                            intensityR = 0;
                            intensityG = 0;
                            intensityB = 0;
                            foreach (IPrimitive primitive1 in primitives)
                                if (primitive1 is Light light1)
                                {
                                    Vector3D lightVector = new Vector3D(light1.X - polyhedronPoint.X,
                                        light1.Y - polyhedronPoint.Y,
                                        light1.Z - polyhedronPoint.Z);
                                    double cos = Vector3D.DotProduct(pointNormal, lightVector)
                                                 / (pointNormal.Length * lightVector.Length);
                                    if (cos >= 0)
                                    {
                                        intensityR += light1.intensity * (light1.Color.R / 255d) * cos;
                                        intensityG += light1.intensity * (light1.Color.G / 255d) * cos;
                                        intensityB += light1.intensity * (light1.Color.B / 255d) * cos;
                                    }
                                }
                            newColor = Color.FromArgb(
                                Math.Max(Math.Min((int)(polyhedronPoint.Color.R * intensityR), 255), newColor.R),
                                Math.Max(Math.Min((int)(polyhedronPoint.Color.G * intensityG), 255), newColor.G),
                                Math.Max(Math.Min((int)(polyhedronPoint.Color.B * intensityB), 255), newColor.B));
                        }
                        polyhedronPoint.Color = newColor;
                    }
                    foreach (Point3D polyhedronPoint in transformedPolyhedron.points)
                    {
                        polyhedronPoint.Apply(camera.Projection);
                        polyhedronPoint.NormalizeToDisplay(bitmap.Width, bitmap.Height);
                    }
                    
                    transformedPrimitives.Add(transformedPolyhedron);
                }
            return transformedPrimitives;
        }
    }
}